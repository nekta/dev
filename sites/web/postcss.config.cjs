const config = require('@config/tailwind/tailwind.config.js');

module.exports = {
  plugins: {
    tailwindcss: { config },
    autoprefixer: {}
  }
}